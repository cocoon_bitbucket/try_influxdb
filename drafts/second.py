"""

annotations


curl -X POST "http://localhost:8086/write?db=mydb&precision=s" --data-binary
'events
    title="Deployed v10.2.0",
    text="<a href='https://github.com'>Release notes</a>",
    tags="these are the tags"
    1470661200'



see https://maxchadwick.xyz/blog/grafana-influxdb-annotations

curl -X POST "http://192.168.99.100:8086/write?db=telegraf&precision=s" --data-binary 'events title="Deployed v1",text="v1",tags="these tags" 1486305053'


"""
import time
from influxdb import InfluxDBClient

print time.time()

def main(host='localhost', port=8086):
    user = 'root'
    password = 'root'
    dbname = 'telegraf'
    dbuser = 'root'
    dbuser_password = 'root'
    query = 'select value from cpu_load_short;'
    json_body = [
        {
            "measurement": "cpu_load_short",
            "tags": {
                "host": "server01",
                "region": "us-west"
            },
            "time": "2009-11-10T23:00:00Z",
            "fields": {
                "Float_value": 0.64,
                "Int_value": 3,
                "String_value": "Text",
                "Bool_value": True
            }
        }
    ]

    client = InfluxDBClient(host, port, user, password, dbname)


    print("Switch user: " + dbuser)
    client.switch_user(dbuser, dbuser_password)

    print("Write points: {0}".format(json_body))
    client.write_points(json_body)

    # write event
    data= 'events title="start_event", text="my_event", tags="these are the tags" %d' % time.time()
    params= {'db':'telegraf'}
    rc= client.write(data, params=params, expected_response_code=204,
          protocol='')


    print("Queying data: " + query)
    result = client.query(query)

    print("Result: {0}".format(result))

    print("Switch user: " + user)
    client.switch_user(user, password)

    print("Drop database: " + dbname)
    client.drop_database(dbname)


def parse_args():
    parser = argparse.ArgumentParser(
        description='example code to play with InfluxDB')
    parser.add_argument('--host', type=str, required=False, default='localhost',
                        help='hostname of InfluxDB http API')
    parser.add_argument('--port', type=int, required=False, default=8086,
                        help='port of InfluxDB http API')
    return parser.parse_args()


if __name__ == '__main__':


    #args = parse_args()

    main(host="192.168.99.100", port=8086)

    print "Done"