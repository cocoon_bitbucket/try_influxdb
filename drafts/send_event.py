import requests
from influxdb import InfluxDBClient
import time
import json
import redis


"""

curl -X POST "http://192.168.99.100:8086/write?db=telegraf" --data-binary 'events title="myEvent",text="hello",tags="these,are,the,tags" 1495624192'


query syntax in grafana to display annotations

select text from events where $timeFilter

"""


headers= {
    "Content-type": "plain/text"
}



s= requests.session()
s.headers=headers




host= "192.168.1.21"
port= 8086
dbname= 'graphite'
user = 'root'
password = 'root'


redis_db=redis.StrictRedis(host=host)



timestamp= str(int(time.time()*1000))
print timestamp



def event_message(title,text,tags="",timestamp=0L):
    """
        compose an Influxdb.event message

    :param title: string
    :param text: string
    :param tags: string
    :param timestamp: float
    :return:
    """
    return dict(
        title= title,
        text= text,
        tags=tags or "",
        timestamp=timestamp or time.time()
    )


def make_influxdb_event_line(msg):
    """

    :param msg: dict ( title,text,tags,timestamp)
    :return:
    """
    # timestamp with precision = ms
    msg["timestamp"] = str(int(msg["timestamp"] * 1000))
    line = 'events title="%(title)s",text="%(text)s",tags="%(tags)s" %(timestamp)s\n' % msg
    return line

def send_event(host,port,event_line,dbname='graphite'):
    """

    :param host:
    :param port:
    :param event_line:
    :return:
    """
    # send with requests
    session = requests.session()
    session.headers = {
        'Content-type': 'application/octet-stream',
        'Accept': 'text/plain'
    }

    url = "http://%s:%d/write" % (host, port)
    params = (
        ('db', dbname),
        ('precision', "ms")
    )

    response = s.post(url, data=event_line, params=params)
    print response.text




title= "another other "
text= "another one"
tags= "keyword,here"


msg= event_message(title=title,text=text,tags=tags)
print msg
msg= json.dumps(msg)
r=redis_db.publish('influxdb.events',msg)


time.sleep(1)
msg= event_message(title=title,text=text,tags=tags)
line= make_influxdb_event_line(msg)


send_event("192.168.1.21",port,event_line=line,dbname="graphite")


# send with influx client
# params = {
#     'db': dbname,
#     'precision': "ms"
# }
# data= [
#     'events title="%s",text="%s",tags="%s" %s' % (title,text,tags,timestamp)
# ]
# client = InfluxDBClient(host, port, user, password, dbname)
# result= client.write(data,params=params,protocol="line")
# assert result is True
# print result


# # send with requests
# session= requests.session()
# session.headers={
#     'Content-type': 'application/octet-stream',
#     'Accept': 'text/plain'
# }
#
# url= "http://%s:%d/write" % (host,port)
# params = (
#     ('db', dbname),
#     ('precision', "ms")
# )
#
# response= s.post(url,data=line,params=params)
# print response.text


print("Done")


