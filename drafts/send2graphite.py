"""

>>> import graphitesend
>>> g = graphitesend.init(prefix='apache.rc', system_name='')
>>> g.send('404', 4)
>>> g.send('200', 500)
The above would send the following metric to graphite

apache.rc.404 4 epoch-time-stamp
apache.rc.200 500 epoch-time-stamp



platform-1.livebox.cpu.total.system


in influx db

database: graphite

select value from "platform-1.livebox.cpu.total.system";





"""
import logging
import random

logging.basicConfig(level=logging.DEBUG)

graphite_server= "192.168.99.100"


import graphitesend

g = graphitesend.init( graphite_server=graphite_server, prefix='platform-1', system_name='livebox')


g.send('cpu.total.user', random.randint(20,50))
g.send('cpu.total.system', random.randint(10,100) * 10)